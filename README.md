# OpenML dataset: Churn-for-Bank-Customers

https://www.openml.org/d/43390

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
Churn for bank customers
RowNumbercorresponds to the record (row) number and has no effect on the output. 
CustomerIdcontains random values and has no effect on customer leaving the bank. 
Surnamethe surname of a customer has no impact on their decision to leave the bank. 
CreditScorecan have an effect on customer churn, since a customer with a higher credit score is less likely to leave the bank.
Geographya customers location can affect their decision to leave the bank. 
Genderits interesting to explore whether gender plays a role in a customer leaving the bank. 
Agethis is certainly relevant, since older customers are less likely to leave their bank than younger ones.
Tenurerefers to the number of years that the customer has been a client of the bank. Normally, older clients are more loyal and less likely to leave a bank.
Balancealso a very good indicator of customer churn, as people with a higher balance in their accounts are less likely to leave the bank compared to those with lower balances.
NumOfProductsrefers to the number of products that a customer has purchased through the bank.
HasCrCarddenotes whether or not a customer has a credit card. This column is also relevant, since people with a credit card are less likely to leave the bank.
IsActiveMemberactive customers are less likely to leave the bank.
EstimatedSalaryas with balance, people with lower salaries are more likely to leave the bank compared to those with higher salaries.
Exitedwhether or not the customer left the bank.

Acknowledgements
As we know, it is much more expensive to sign in a new client than keeping an existing one.
It is advantageous for banks to know what leads a client towards the decision to leave the company.
Churn prevention allows companies to develop loyalty programs and retention campaigns to keep as many customers as possible.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43390) of an [OpenML dataset](https://www.openml.org/d/43390). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43390/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43390/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43390/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

